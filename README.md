# Projekt

Napravio sam igru Yahtzee. Nisam stavio granicu na broju igrača, tako da može igrat više ljudi, a može i solo. 
Yahtzee se inače igra iz 13 rundi, ali možete promijeniti u game.cpp-u(Linija 23) da se igra i iz manje rundi(zbog testiranja). 
Igra se tako da kad igrač dobije kockice može birati hoće li rollati ili holdati. 
Ako izabere roll onda će ga program pitati koliko kockica želi zadržati, a zatim će igrač upisati brojeve kockica koje želi zadržati(1-5).
Zatim će se generirati nove random kockice onih koje igrač nije zadržao. Također može u svakom roll-u "osloboditi" kockice koje je u pethodnim roll-ovima zadržao.
Igrač ima opciju roll-ati dva puta. 
Ako igrač izabere hold opciju to znači da je zadovoljan sa svojim kockicama i bira u koji će red upisati bodove. 
Nakon izabira broja (1-13) program sam zbraja i upisuje bodove u izabrani red. Igrač ne može dva puta upisati bodove u isti red, program će ga obavijestiti i dat mu opciju da ponovno upiše.
Nakon svake ruke program će zbrojiti bodove iz tablice i pridodati ih totalnom zbroju igračevih bodova.
Nakon što završe sve ruke program će upisati ukupan broj bodova svakog igrača. 
