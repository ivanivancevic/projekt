#include "game.h"
#include "player.h"
#include <iostream>
#include <time.h>
#include <vector>

using namespace std;

Game::Game()
{
    string name, choice;
    int num_players;
    cout << "Unesite broj igraca" << endl;
    cin >> num_players;
    Player* players = new Player[num_players];
    while(num_players)
    {
        this->round = 0;
        cout << "Unesite ime igraca : ";
        cin >> name;
        players[num_players-1].set_player_name(name);
        players[num_players-1].start();
        while(this->round!=13)     // Promijenite broj rundi zbog testiranja
        {
            players[num_players-1].start();
            players[num_players-1].print_dices();
            cout << "Zelite li rollati ili holdati?(napisite roll ili hold)" << endl;
            cin >> choice;
            if(choice == "hold")
            {
                players[num_players-1].hold();
                this->round++;
                continue;
            }
            players[num_players-1].roll();
            players[num_players-1].print_dices();
            cout << "Zelite li rollati ili holdati?(napisite roll ili hold)" << endl;
            cin >> choice;
            if(choice == "hold")
            {
                players[num_players-1].hold();
                this->round++;
                continue;
            }
            players[num_players-1].roll();
            players[num_players-1].print_dices();
            players[num_players-1].print_section_points();
            players[num_players-1].set_section_points();
            players[num_players-1].print_section_points();
            players[num_players-1].print_player_points();
            this->round++;
        }
        num_players--;
    }
    delete[] players;
}

Game::~Game()
{
    cout << "\nGame over!" << endl;
}
