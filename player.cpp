#include "player.h"

using namespace std;

Player::Player()
{
    this->player_points = 0;
    this->player_name = "PlayerX";
}
Player::Player(string name)
{
    this->player_points = 0;
    this->player_name = name;
}

Player::~Player()
{
    cout << "\nIgrac " << this->get_player_name() << " je ostvario " << this->get_player_points() << " bodova!" << endl;
}

void Player::start()
{
    srand (time(NULL));
    for(int i=0 ; i<5 ; i++)
    {
        this->dices[i] = (rand() % 6 + 1);
    }
}

void Player::set_player_points(int number)
{
    this->player_points += number;
}

int Player::get_player_points()
{
    return this->player_points;
}

void Player::roll()
{
    int card_num, temp;
    vector<int> choices;
    cout << "Upisite koliko kocki zelite zadrzati : ";
    cin >> card_num;
    if(card_num && card_num != 5)
    {
        cout << "Upisite koje kocke zelite zadrzati(nakon svake pritisnite Enter)" << endl;
        while(card_num)
        {
            cin >> temp;
            choices.push_back(temp);
            card_num--;
        }
    }
    if(choices.size())
    {
        for(int i = 0 ; i < choices.size() ; i++)
            holding_dices[choices[i]-1] = 1;
    }

    srand (time(NULL));
    if(card_num != 5)
    {
        for(int i = 0 ; i<5 ; i++)
        {
            if(!this->holding_dices[i])
                this->dices[i] = (rand() % 6 + 1);
        }
    }
    for(int i = 0 ; i<5 ; i++)
        this->holding_dices[i] = 0;
}

void Player::print_dices()
{
    cout << "Ovo su kocke igraca " << this->get_player_name() << endl;
    for(int i = 0 ; i<5 ; i++)
    {
        cout << shapes[dices[i]-1];
    }
    cout << "\n";
}

void Player::hold()
{
    this->print_section_points();
    this->print_player_points();
    this->set_section_points();
    this->print_section_points();
    this->print_player_points();
}

void Player::print_section_points()
{
    for(int i = 0 ; i<13 ; i++)
    {
        cout << i+1 << ") "<< sections[i] << " " << section_points[i] << endl;
    }
}

void Player::set_player_name(string name)
{
    this->player_name = name;
}

void Player::set_section_points()
{
    int points=0, section;
    cout << "Upisite gdje zelite upisati bodove" << endl;
    cin >> section;
    if(section_points[section-1])
    {
        cout << "Ovdje su vec upisani brojevi!" << endl;
        set_section_points();
    }

    if(section == 0)
    {
        cout << "Ta seckija ne postoji !" << endl;
        set_section_points();
    }

    if(section == 1 || section == 2 || section == 3 || section == 4 || section == 5 || section == 6)
    {
        for(int i=0;i<5;i++)
        {
            if(dices[i] == section)
                points+=section;
        }
    }

    else if(section == 7)
    {
        sort(dices, dices+5);
        for(int i = 0 ; i < 4 ; i++)
        {
            if(dices[i]==dices[i+1] && dices[i+1]==dices[i+2])
            {
                for(int j=0 ; j < 5 ; j++)
                    points += dices[j];
            }
        }
        if(points==0)
        {
            cout << "Nemate Three Of A Kind!" << endl;
            set_section_points();
        }
    }

    else if(section == 8)
    {
        sort(dices, dices+5);
        for(int i = 0 ; i < 4 ; i++)
        {
            if(dices[i]==dices[i+1] && dices[i+1]== dices[i+2] && dices[i+2] == dices[i+3])
            {
                for(int j=0 ; j < 5 ;j++)
                    points += dices[j];
            }
        }
        if(points==0)
        {
            cout << "Nemate Four Of A Kind!" << endl;
            set_section_points();
        }
    }

    else if(section == 9)
    {
        sort(dices, dices+5);
        int c1=0, c2=0;
        if(dices[0] == dices[1] && dices[2]==dices[3] && dices[3]==dices[4])
        {
            points = 25;
        }
        else if (dices[0] == dices[1] && dices[1] == dices[2] && dices[3==4])
        {
            points = 25;
        }
        else
        {
            cout << "Nemate Full House!" << endl;
            set_section_points();
        }
    }

    else if(section == 10)
    {
        sort(dices, dices+5);
        int counter=0;
        for(int i=0;i<4;i++)
        {
            if(counter==3)
            {
                points=30;
                continue;
            }
            if(dices[i]+1==dices[i+1])
            {
                counter++;
            }
            else
                counter=0;
        }
        if(points!=30)
        {
            cout << "Nemate Small Straight!" << endl;
            set_section_points();
        }
    }
    else if(section == 11)
    {
        sort(dices, dices+5);
        for(int i=0;i<4;i++)
        {
            if(dices[i]+1!=dices[i+1] )
            {
                cout << "Nemate Large Straight!" << endl;
                set_section_points();
            }
        }
        points = 40;
    }

    else if(section == 12)
    {
        for(int i=0;i<5;i++)
        {
            if(dices[i]!=dices[0])
            {
                cout << "Ne mozete zvati Yahtzee jer nemate 5 istih kocki!" << endl;
                set_section_points();
            }

        }
        points = 50;
    }

    else if(section == 13)
    {
        for(int i = 0 ; i < 5 ; i++)
        {
            points += dices[i];
        }
    }

    this->set_player_points(points);
    section_points[section-1] = points;
}

void Player::print_player_points()
{
    cout << "Total = " << this->player_points << endl;;
}

string Player::get_player_name()
{
    return this->player_name;
}
