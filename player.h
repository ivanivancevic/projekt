#ifndef PLAYER_H
#define PLAYER_H
#include <iostream>
#include <time.h>
#include <vector>
#include <string>

using namespace std;

class Player{
    string player_name;
    int player_points;
    int dices[5];
    int holding_dices[5] = {};
    int section_points[13] = {};
    string sections[13] = {"Aces", "Twos", "Threes", "Fours", "Fives", "Sixes", "Three Of A Kind", "Four Of A Kind", "Full House", "Small Straight", "Large straight", "Yahtzee", "Chance"};
    string shapes[6] = {"\n ------- \n|       |\n|       |\n|   o   |\n|       |\n|       |\n ------- \t", "\n ------- \n|       |\n|    o  |\n|       |\n|  o    |\n|       |\n ------- \t", "\n ------- \n|       |\n|    o  |\n|   o   |\n|  o    |\n|       |\n ------- \t", "\n ------- \n|       |\n|  o o  |\n|       |\n|  o o  |\n|       |\n ------- \t", "\n ------- \n|       |\n|  o o  |\n|   o   |\n|  o o  |\n|       |\n ------- \t", "\n ------- \n|       |\n|  o o  |\n|  o o  |\n|  o o  |\n|       |\n ------- \t"};
public:
    Player();
    Player(string name);
    ~Player();
    string get_player_name();
    void start();
    void set_player_points(int number);
    void set_player_name(string name);
    int get_player_points();
    void print_player_points();
    void roll();
    void print_dices();
    void hold();
    void set_section_points();
    void print_section_points();
};
#endif // PLAYER_H
